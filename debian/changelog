libvigraimpex (1.11.1+dfsg-12) UNRELEASED; urgency=medium

  * Set extend-diff-ignore="^doc/" to fix building twice from the same
    source. Closes: #1046259

 -- Andreas Metzler <ametzler@debian.org>  Tue, 15 Aug 2023 13:34:28 +0200

libvigraimpex (1.11.1+dfsg-11) unstable; urgency=medium

  * 54_use_tuples_for_numpy_indexing.diff and 55_no_bool_import_numpy.diff:
    Fix build error with numpy 1.23. Closes: #1026487
  * 56_python311.diff Fix build error with python 3.11.
  * [lintian] b-d on libtiff-dev instead of libtiff5-dev.
  * Run wrap-and-sort -a -s -t.
  * Add Built-Using: ${sphinxdoc:Built-Using} to python-vigra-doc.

 -- Andreas Metzler <ametzler@debian.org>  Sat, 21 Jan 2023 14:07:30 +0100

libvigraimpex (1.11.1+dfsg-10) unstable; urgency=medium

  * Rename patches for filename alphanumic order = order of application.
  * 52_vigra-openexr3.patch: from Fedora, fix build against openexr3.
    Closes: #1017550

 -- Andreas Metzler <ametzler@debian.org>  Fri, 19 Aug 2022 20:31:28 +0200

libvigraimpex (1.11.1+dfsg-9) unstable; urgency=medium

  [ Andreas Metzler ]
  * 48_sphinx4_error.diff by Omari Stephens: Fix build error with sphinx 4.

  [ Heinz Repp ]
  * c++17conf.patch: Fix Build issue with gcc-11. Partial fix for #984049

  [ Florian Ernst ]
  * python_3.10_support.patch: Fix test error in C++ template syntax.
    Closes: #984049

 -- Andreas Metzler <ametzler@debian.org>  Sun, 03 Apr 2022 08:01:37 +0200

libvigraimpex (1.11.1+dfsg-8) unstable; urgency=medium

  * Use Build-Depends-Indep: dh-sequence-sphinxdoc instead of
    override_dh_installdocs-indep in debian/rules. Add b-d on debhelper (>=
    12.5).
  * Drop --with python3 and replace b-d on dh-python with a b-d on
    dh-sequence-python3.
  * Switch to B-D on dh-sequence-numpy3, drop --with numpy3 dh option.
  * Rename patches to let thmem sort correctly by filename.
  * 47_fileencoding-utf-8.diff: Re-encode two files from ISO-8859-1 to UTF-8
    to fix FTBFS. Closes: #980630
  * Use watchfile v4 standard.
  * Use dh v13 compat.

 -- Andreas Metzler <ametzler@debian.org>  Sun, 31 Jan 2021 13:06:24 +0100

libvigraimpex (1.11.1+dfsg-7) unstable; urgency=medium

  * Avoid blhc (Buildd Log Scanner) false positives by appending instead of
    replacing -O options for specific tests.
  * Simplify debian/rules, drop double-build with/without python.
  * 1.11.1+dfsg-3 replaced the symlink /usr/share/doc/libvigraimpex-doc/html
    with a directory (introduced as part of DH 12 upgrade). Use
    debian/libvigraimpex-doc.maintscript to properly handle upgrades
    from << 1.11.1+dfsg3. (Thanks, Andreas Beckmann!) Closes: #950111
    Also retroactively fix systems already upgraded from << -3 to <= -6 by
    deleting the symlink /usr/share/doc/libvigraimpex-dev/html and moving
    /usr/share/doc/libvigraimpex-doc/html to /usr/share/doc/libvigraimpex-dev
    in libvigraimpex-doc.preinst.

 -- Andreas Metzler <ametzler@debian.org>  Thu, 06 Feb 2020 15:43:35 +0100

libvigraimpex (1.11.1+dfsg-6) unstable; urgency=medium

  * rules: Drop -DBoost_PYTHON_LIBRARY_RELEASE argument, it is not used
    anymore.
  * B-d on python3-dev instead of python3-all-dev since only the python
    module for the default python version is built. (Thanks, Sebastiaan
    Couwenberg!)
  * Adopt package. Closes: #945193

 -- Andreas Metzler <ametzler@debian.org>  Fri, 03 Jan 2020 14:36:25 +0100

libvigraimpex (1.11.1+dfsg-5) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.

 -- Andreas Metzler <ametzler@debian.org>  Fri, 27 Dec 2019 17:39:59 +0100

libvigraimpex (1.11.1+dfsg-4) experimental; urgency=medium

  * QA upload.
  * Improve readability of compilation flag settings.
  * Revamp rules, use dh sequencer. Install docs from installed (debian/tmp)
    instead of built directory.

 -- Andreas Metzler <ametzler@debian.org>  Wed, 25 Dec 2019 14:34:19 +0100

libvigraimpex (1.11.1+dfsg-3) experimental; urgency=medium

  [ Daniel Stender ]
  * Orphaning package.

  [ Andreas Tille ]
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Repository.
  * Fix day-of-week for changelog entries 1.4.0-1.
  * Build Python3 package instead of Python2
    Closes: #852052, #936932

  [ Andreas Metzler ]
  * QA upload.
  * vigra-1.11.1.py37.patch from upstream GIT: Fix FTBFS with python3.
  * Python Debian build:
    + Rename python-vigra.install to python3-vigra.install to follow
      package rename and get a nonempty result.
    + Fix build. As a first try only build for current default python version
      (py3versions -d) since upstream installs to /usr/lib/python3 instead of
      /usr/lib/pythonX.Y.
  * cmake-add-boost_python-MAJOR-MINOR-fixes-445.patch from upstream GIT: Fix
    boost python detection.
  * Fix dh-12 build error (non-matching wildcards) by updating
    libvigraimpex-dev.examples.
  * Add b-d-i for texlive-latex-extra (needed for newunicodechar.sty).
  * ftbfs-lenna-removal.diff: Fix build error due to lenna removal.
  * ftbfs-python3-doc-generation.diff: Python3 build error in doc generation.
  * DH 12 installs libvigraimpex-doc files directly to
    /usr/share/doc/libvigraimpex-dev. Adapt rules, update doc-base entry and
    drop symlink.
  * Set Rules-Requires-Root: no.
  * Tighten python3-vigra --> libvigraimpex11 dependency.

 -- Andreas Metzler <ametzler@debian.org>  Mon, 23 Dec 2019 18:20:46 +0100

libvigraimpex (1.11.1+dfsg-2) experimental; urgency=medium

  * add sphinx.ext.pngmath.patch by Andreas Tille.

 -- Daniel Stender <stender@debian.org>  Sun, 10 Mar 2019 20:07:43 +0100

libvigraimpex (1.11.1+dfsg-1) experimental; urgency=medium

  * New upstream release (Closes: #863063):
    + unfuzz disable-tests-which-would-need-lenna-images.diff.
    + drop fix-impex-positiontest.diff (applied upstream).
    + unfuzz docsrc-paths.diff.
    + drop const-swap.patch (applied upstream).
    + update, rename multi_convolution_fix_incomplete_template_parameter.patch.
    + build libvigraimpex11 (changes in d/control and d/rules).
  * deb/copyright:
    + drop .hgignore and .hgtags from Files-Excluded.
    + use Expat instead of MIT.

 -- Daniel Stender <stender@debian.org>  Sat, 29 Dec 2018 08:16:16 +0100

libvigraimpex (1.10.0+git20160211.167be93+dfsg1-2) unstable; urgency=medium

  * Team upload.
  * Replace sphinx.ext.pngmath by sphinx.ext.imgmath
    Closes: #923467

 -- Andreas Tille <tille@debian.org>  Fri, 08 Mar 2019 18:40:38 +0100

libvigraimpex (1.10.0+git20160211.167be93+dfsg1-1) unstable; urgency=medium

  * Repackaged upstream tarball.
  * deb/changelog:
    + remove a trailing line.
  * deb/control:
    + bump standards to 4.3.0.
    + drop ancient X-Python-Version field.
  * deb/copyright:
    + exclude src/images/dissolved.gif (another "lenna" image).
    + exclude docsrc/vigra-wiki.html (not included in docs package).
  * deb/rules:
    + drop obsolete DEB_BUILD_OPTIONS export line.
    + don't evaluate DEB_BUILD_ARCH manually, but include architecture.mk.
  * deb/python-vigra-doc.doc-base:
    + fix Text paragraph for doc-base-file-references-missing-file.
  * bump debhelper level to 10 (changes in d/control and d/compat).

 -- Daniel Stender <stender@debian.org>  Thu, 27 Dec 2018 22:30:27 +0100

libvigraimpex (1.10.0+git20160211.167be93+dfsg-6.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New patch link-with-pthread.patch to fix FTBFS against boost1.67 on
    armel (Closes: #914649)

 -- Gilles Filippini <pini@debian.org>  Thu, 29 Nov 2018 20:39:23 +0100

libvigraimpex (1.10.0+git20160211.167be93+dfsg-6) unstable; urgency=medium

  * add multi_convolution_fix_incomplete_template_paramater.patch
    (Closes: #897800).
  * deb/changelog: remove a trailing whitespace.
  * deb/control: update Vcs fields (moved to salsa).
  * deb/copyright:
    + use HTTPS in Format field.
    + expand copyright span.
  * add deb/gbp.conf.

 -- Daniel Stender <stender@debian.org>  Sat, 11 Aug 2018 06:20:04 +0200

libvigraimpex (1.10.0+git20160211.167be93+dfsg-5) unstable; urgency=medium

  * deb/rules: build test_multiarray with -O0 (Closes: #872037).

 -- Daniel Stender <stender@debian.org>  Mon, 14 Aug 2017 22:40:53 +0200

libvigraimpex (1.10.0+git20160211.167be93+dfsg-4) unstable; urgency=medium

  * Upload to unstable.
  * deb/rules: add info on manipulation of test_gridgraph.

 -- Daniel Stender <stender@debian.org>  Sat, 12 Aug 2017 23:00:23 +0200

libvigraimpex (1.10.0+git20160211.167be93+dfsg-4~exp1) experimental; urgency=medium

  * const-swap.patch: provide DEP-3 header.
  * deb/control: update Homepage.
  * deb/rules:
    + change test_blockwiselabeling and test_blockwisewatersheds
      to build with -O0 (Closes: #853513) [thanks to Andreas Metzler].
    + apply the same change for test_gridgraph.
  * drop temporarily-disable-blockwisealgorithms-tests.patch (not needed
    anymore).

 -- Daniel Stender <stender@debian.org>  Thu, 10 Aug 2017 13:02:27 +0200

libvigraimpex (1.10.0+git20160211.167be93+dfsg-3) unstable; urgency=medium

  * add temporarily-disable-blockwisealgorithms-tests.patch.
  * add const-swap.patch (Closes: #871169) [thanks to Matthias Klose].
  * deb/copyright: add src/images/dissolved_color.gif to Files-Excluded
    (which contains "lenna" in composite).

 -- Daniel Stender <stender@debian.org>  Wed, 09 Aug 2017 00:41:15 +0200

libvigraimpex (1.10.0+git20160211.167be93+dfsg-2) unstable; urgency=medium

  * deb/rules: set ffloat-store for i386 CPUs (Closes: #820429)
    [thanks to Tobias Frost].

 -- Daniel Stender <stender@debian.org>  Mon, 18 Apr 2016 17:37:49 +0200

libvigraimpex (1.10.0+git20160211.167be93+dfsg-1) unstable; urgency=medium

  * Stripped non-free "lenna" images from source tarball (Closes: #794853):
    + added all image instances (inkl. Khoros Visualization images, .xv) to
      Files-Excluded in deb/copyright.
    + added disable-tests-which-would-need-lenna-images.diff.

 -- Daniel Stender <stender@debian.org>  Wed, 23 Mar 2016 21:12:41 +0100

libvigraimpex (1.10.0+git20160211.167be93-4) unstable; urgency=medium

  * Upload to unstable.
  * deb/changelog: merged with Sid.
  * deb/rules: build test/math with -O0 (Closes: #817840), cosmetics.

 -- Daniel Stender <stender@debian.org>  Sat, 12 Mar 2016 08:39:03 +0100

libvigraimpex (1.10.0+git20160211.167be93-3) experimental; urgency=medium

  * deb/rules: temporarily failsafe test/multiarray/test_multiarray_chunked
    to build on ppc64el.

 -- Daniel Stender <stender@debian.org>  Wed, 09 Mar 2016 21:45:05 +0100

libvigraimpex (1.10.0+git20160211.167be93-2) experimental; urgency=medium

  * deb/control: added missing Breaks for libvigraimpex6 (Closes: #815795).
  * Added fix-impex-positiontest.diff (Closes: #812850)
  * deb/rules:
    + build with Boost.Thread on armel (Closes: #814606) [plus, added
      libboost-thread-dev to build-deps in control; thanks to Ullrich Koethe].
    + dropped failsafe for test/impex (not needed anymore).
  * Updated uploader email address.

 -- Daniel Stender <stender@debian.org>  Tue, 01 Mar 2016 23:32:30 +0100

libvigraimpex (1.10.0+git20160211.167be93-1) experimental; urgency=medium

  [ Daniel Stender ]
  * Build libvigraimpex6 (Closes: #813415).
  * Add support for multiarch (Closes: #812560).
    + don't ship libvigraimpex-dev with old style vigra-config script.
  * deb/control:
    + depend libvigraimpex-dev on libpng-dev [thanks to Tobias Frost].
    + use HTTPS for Vcs fields.
    + bumped Standards-Version to 3.9.7 (no changes needed).
    + added Replaces to libvigraimpex6 [thanks to Andreas Metzler].
  * deb/copyright: expanded copyright span, added email address.
  * deb/rules:
    + added DH_VERBOSE export line.
    + add BINDNOW to linker flags.
    + don't build with -O3 on ppc64el (support for Ubuntu development).
    + dropped --keep-going from testsuite call to break immediately.
    + removed old, obsolete and unneeded stuff, cosmetics.
  * deb/clean: removed unneeded lines.
  * deb/README.Debian: corrected typo.
  * Dropped deb/libvigraimpex5v5.lintian-overrides.

  [ Andreas Metzler ]
  * Build with version-1-11-rc branch.
    + Unfuzz patches.
    + deb/control: version libboost-python-dev build-dependency.
    + deb/rules: tighten dh_makeshlibs -V argument.

 -- Daniel Stender <debian@danielstender.com>  Mon, 15 Feb 2016 02:43:41 +0100

libvigraimpex (1.10.0+git20160120.803d5d4-3) unstable; urgency=medium

  * deb/control: depend libvigraimpex-dev on libpng-dev.

 -- Daniel Stender <debian@danielstender.com>  Tue, 23 Feb 2016 15:25:11 +0100

libvigraimpex (1.10.0+git20160120.803d5d4-2) unstable; urgency=medium

  * Build libvigraimpex6 (Closes: #813415).

 -- Daniel Stender <debian@danielstender.com>  Sat, 20 Feb 2016 15:48:35 +0100

libvigraimpex (1.10.0+git20160120.803d5d4-1) unstable; urgency=medium

  * Packaging latest development snapshot (Closes: #811370).
  * deb/control:
    + needs python3-sphinx to build.
  * deb/rules:
    + failsafe test/impex temporarily due for problems with EXR.
  * Build with different upstream tarball (the flavour without doc/):
    + deb/watch: changed regex pattern for tarball, added repacksuffix.
    + dropped deb/get-orig.source.sh.
    + deb/copyright: added Files-Excluded with remaining strip patterns.
    + deb/rules: dropped build target get-orig-source, updated clean target.
  * Dropped:
    + deb/*.dirs (obsolete)
    + off-by-on-in-exportImageAlpha.diff (applied upstream).
  * Updated:
    + docsrc-out-of-source.diff, docsrc-paths.diff, removed-static-docs.diff.

 -- Daniel Stender <debian@danielstender.com>  Tue, 26 Jan 2016 22:38:23 +0100

libvigraimpex (1.10.0+dfsg-11) unstable; urgency=medium

  * Upload to unstable.

 -- Daniel Stender <debian@danielstender.com>  Thu, 29 Oct 2015 22:31:53 +0100

libvigraimpex (1.10.0+dfsg-10) experimental; urgency=medium

  * rename libvigraimpex5 to libvigraimpex5v5 for the libstdc++6 transition.

 -- Daniel Stender <debian@danielstender.com>  Fri, 14 Aug 2015 23:56:26 +0200

libvigraimpex (1.10.0+dfsg-9) experimental; urgency=medium

  * deb/control:
    + updated Maintainer and Uploaders (packaged moved to Debian Science).
    + added Vcs-Browser and Vcs-Git.
  * deb/copyright:
    + updated Source: (upstream moved to Github).
  * deb/rules:
    + run dh_numpy w/o script (deb/dh_numpy dropped) [Andreas Tille].
    + corrected switch for ffloat-store flag on i386.
  * added docs-no-footer-timestamp.diff (Closes: #783230).

 -- Daniel Stender <debian@danielstender.com>  Tue, 28 Jul 2015 20:19:42 +0200

libvigraimpex (1.10.0+dfsg-8) experimental; urgency=medium

  * deb/control:
    + change of maintainer (Closes: #587063).
    + bumped Debhelper level to 9 (also in deb/compat).
    + added dh-python to b-deps.
    + depend on libtiff5-dev (Closes: #681077).
    + bumped standards to 3.9.6 (no changes needed).
  * deb/copyright:
    + added DEP-5 compliant Format line.
    + removed trailing commas, indented, sorted properly; updated.
  * deb/rules:
    + straightened arch dependend flag generation.
    + removed Vim control line.
  * deb/watch: updated to Github repo.
  * Patches:
    + removed-static-docs.diff: added DEP-3 compliant header.
    + dropped disable-noise-variance-clustering-vigranumpy-test.diff
      (not needed anymore, Closes: #588167).
    + added off-by-on-in-exportImageAlpha.diff.
  * wrap-and-sort -a -s.

 -- Daniel Stender <debian@danielstender.com>  Wed, 11 Feb 2015 17:13:23 +0100

libvigraimpex (1.10.0+dfsg-7) experimental; urgency=medium

  * QA upload.
  * Support hdf5 1.8.13 new packaging layout. (Patch by Gilles Filippini)
    Closes: #756677

 -- Andreas Metzler <ametzler@debian.org>  Sat, 02 Aug 2014 14:31:14 +0200

libvigraimpex (1.10.0+dfsg-6) experimental; urgency=medium

  * QA upload.
  * Rebuild with new toolchain (gcc-4.9).

 -- Andreas Metzler <ametzler@debian.org>  Sat, 26 Jul 2014 14:00:42 +0200

libvigraimpex (1.10.0+dfsg-5) experimental; urgency=medium

  * QA upload.
  * Use ggc-min-expand for mips.
    Patch by Dejan Latinovic <Dejan.Latinovic@imgtec.com>.
    Closes: #750418.

 -- Anibal Monsalve Salazar <anibal@debian.org>  Wed, 11 Jun 2014 08:31:01 +0100

libvigraimpex (1.10.0+dfsg-4) experimental; urgency=medium

  * Reupload to experminental to test buildability with newer toolchain.
    This version does not FTBFS on i386 at least. Closes: #750418

 -- Andreas Metzler <ametzler@debian.org>  Mon, 09 Jun 2014 07:37:12 +0200

libvigraimpex (1.10.0+dfsg-3) experimental; urgency=medium

  * QA upload.
  * Build with -pipe.

 -- Andreas Metzler <ametzler@debian.org>  Mon, 27 Jan 2014 17:27:13 +0100

libvigraimpex (1.10.0+dfsg-2) experimental; urgency=medium

  * QA upload.
  * (Build-)Depend on libhdf5-dev instead of removed transitional package
    libhdf5-serial-dev. Closes: #735569
  * (Build-)Depend on libtiff-dev instead of transitional package
    libtiff4-dev.

 -- Andreas Metzler <ametzler@debian.org>  Sat, 18 Jan 2014 16:23:38 +0100

libvigraimpex (1.10.0+dfsg-1) experimental; urgency=low

  * QA upload.
  * Update watchfile.
  * Use xz compression for source tarball.
  * New upstream version.
    + Drop patches previously pulled from upstream
      20_increased-tolerance-tests.diff
      ref-to-temporary-in-Sampler.diff
    + soname bumped, change package name.
    + Filename of upstream README changed.

 -- Andreas Metzler <ametzler@debian.org>  Sat, 07 Dec 2013 19:56:15 +0100

libvigraimpex (1.9.0+dfsg-7) unstable; urgency=low

  * QA upload.
  * Make debian/rules work with both multiarch and non-multiarch
    libboost-python. (This supersedes the change in
    Ubuntu 1.9.0+dfsg-6ubuntu2.)

 -- Andreas Metzler <ametzler@debian.org>  Sat, 05 Oct 2013 13:45:19 +0200

libvigraimpex (1.9.0+dfsg-6) unstable; urgency=low

  * QA upload.
  * Drop parallell build support.

 -- Andreas Metzler <ametzler@debian.org>  Fri, 05 Jul 2013 19:05:49 +0200

libvigraimpex (1.9.0+dfsg-5) unstable; urgency=low

  * Also append CPPFLAGS to CXXFLAGS.
  * Increase minimum memory requirement for parallel build on buildd by factor
    three since s390 exhausted virtual memory on parallel build.

 -- Andreas Metzler <ametzler@debian.org>  Thu, 04 Jul 2013 19:08:27 +0200

libvigraimpex (1.9.0+dfsg-4) unstable; urgency=low

  * QA upload.
  * Switch back to tiff4, we do not want to start our own
    mini-tiff-transition.
  * Upload to unstable.

 -- Andreas Metzler <ametzler@debian.org>  Sun, 16 Jun 2013 13:57:23 +0200

libvigraimpex (1.9.0+dfsg-3) experimental; urgency=low

  * QA upload.
  * [ref-to-temporary-in-Sampler.diff] Fix FTBFS witch gcc-4.8.
    Closes: #701313

 -- Andreas Metzler <ametzler@debian.org>  Sun, 19 May 2013 15:44:09 +0200

libvigraimpex (1.9.0+dfsg-2) experimental; urgency=low

  * QA upload.
  * [20_increased-tolerance-tests.diff] from upstream GIT: Increase tolerance
    for a few tests hopefully fixing testsuite errors on s390x and ia64.

 -- Andreas Metzler <ametzler@debian.org>  Tue, 23 Apr 2013 19:10:22 +0200

libvigraimpex (1.9.0+dfsg-1) experimental; urgency=low

  * QA upload.
  * New upstream version.
  * Partial sync from Ubuntu:
    + Unfuzz patches docsrc-out-of-source.diff and docsrc-paths.diff
    + Drop no-offline-ps-doc.diff, doxygen-1.8.diff and gcc-4.7-fixes.diff,
      included upstream.
    + Drop gzipped-tutorial-reference.diff, pointing to upstream copy instead.
    + Downgrade python-h5py from python-vigra's Recommends to Suggests.
  * Use $(wildcard) to find both multiarched and regular libpython.
  * Explicitly enable OpenEXR-support, it is not selected automatically
    anymore.
  * Bump shlibs.

 -- Andreas Metzler <ametzler@debian.org>  Sun, 07 Apr 2013 13:38:04 +0200

libvigraimpex (1.8.0+dfsg-2) experimental; urgency=low

  * QA upload.
  * Enable OpenEXR support by adding a build-dependency on libopenexr-dev.
  * Stop using .symbols files, and use 'dh_makeshlibs -V ...' to version the
    shlibs file. This fixes FTBFS with diffferent gcc != 4.7.
    Closes: #691095

 -- Andreas Metzler <ametzler@debian.org>  Sun, 13 Jan 2013 13:10:39 +0100

libvigraimpex (1.8.0+dfsg-1) experimental; urgency=low

  * QA upload.
  * New upstream version.
  * Update patches for new upstream release.
  * Bump libvigraimpex soname to 4, update symbols file.
  * Fix (new?) GCC 4.7 build failures.
  * Fix build failure with doxygen 1.8.x.
  * Build using dh_python2.
  * debian/rules: Remove -mieee for sh4, it's now the default.
  * Build with -ffloat-store on i386, to let the testsuite pass.
  * Don't stop running the testsuite on the first error.
  * On powerpc, run the tests, but ignore the test results.
    It's likely that the build will fail on other archs, after
    inspecting the test results, the outcome of the test should be
    disabled or the precision for some tests be adjusted.

 -- Matthias Klose <doko@debian.org>  Wed, 26 Sep 2012 23:52:37 +0200

libvigraimpex (1.7.1+dfsg1-3) unstable; urgency=low

  * QA upload
  * Fix build failure with GCC 4.7. Closes: #667258.
  * Build-depend on libpng-dev. Closes: #662414.

 -- Matthias Klose <doko@debian.org>  Mon, 16 Apr 2012 23:06:00 +0200

libvigraimpex (1.7.1+dfsg1-2) unstable; urgency=low

  * QA upload
  * Patch libpng15: Fix FTBFS with libpng 1.5 (closes: #636003). Thanks to
    Nobuhiro Iwamatsu and Joachim Schleicher for forwarding and testing
    the patch.
  * Replace build-dependency on libpng12-dev by libpng-dev | libpng12-dev
    in order to ease the upcoming libpng transition.
  * Fix FTBFS on sh4 architecture by using -mieee flag (closes: #633818).
    Thanks to Nobuhiro Iwamatsu for the patch!

 -- Ralf Treinen <treinen@debian.org>  Thu, 04 Aug 2011 13:57:08 +0200

libvigraimpex (1.7.1+dfsg1-1) unstable; urgency=low

  * QA upload.
  * Set maintainer to Debian QA Group.
  * Demote Recommends of the documentation packages to Suggests.
  * Drop fix-tinyvectortest.diff, no longer needed.
  * Drop Vcs-* fields.
  * Strip jQuery code from .orig.tar.gz, as we don't have source for it.
    + Update debian/get-orig-source.sh, debian/rules, debian/watch.
    + Update debian/copyright.
  * Overhaul debian/rules. Stop using dh.
  * Use dh_sphinxdoc:
    + Update debian/control to use ${sphinxdoc:Depends} instead of hardcoded
      libjs-jquery.
    + Call it in debian/rules if it's installed. Otherwise, generate substvars
      manually.
  * Add missing build-dependency on python-support.

 -- Jakub Wilk <jwilk@debian.org>  Tue, 12 Jul 2011 19:34:24 +0200

libvigraimpex (1.7.1+dfsg-8) unstable; urgency=low

  * Unbreak fix-tinyvectortest.diff.

 -- Jakub Wilk <jwilk@debian.org>  Wed, 25 May 2011 01:21:31 +0200

libvigraimpex (1.7.1+dfsg-7) unstable; urgency=low

  * To fix FTBFS on s390, rewrite a loop in TinyVectorTest::equalIter(), so
    that it doesn't trigger bug in GCC 4.6 (see Debian bug #627795).
    [fix-tinyvectortest.diff]

 -- Jakub Wilk <jwilk@debian.org>  Tue, 24 May 2011 22:09:12 +0200

libvigraimpex (1.7.1+dfsg-6) unstable; urgency=low

  * To fix FTBFS on powerpc with GCC 4.6, use epsilon comparison in
    ConvolutionTest::recursiveFilterTestFromRepeatWithAvoid. Thanks to Stefano
    Rivera for debugging and for the patch. [fix-convolutiontest.diff]
  * Allow libvigraimpex-dev to be co-installable with over providers of
    libhdf5-dev than libhdf5-serial-dev (closes: #627421). Thanks to Laurent
    Bonnaud for the bug report.

 -- Jakub Wilk <jwilk@debian.org>  Fri, 20 May 2011 20:24:30 +0200

libvigraimpex (1.7.1+dfsg-5) unstable; urgency=low

  * Use default Python version when building documentation.
  * Update docsrc-doxygen-1.7.diff to deal with the latest Doxygen.
  * Remove unused jquery.js from libvigraimpex-doc.
  * Add a missing include to fix FTBFS with GCC 4.6 (closes: #625104). Thanks
    to Lucas Nussbaum for the bug report. [fix-ftbfs-gcc4.6.diff]
  * Bump standards version to 3.9.2 (no changes needed).
  * Mark more symbols as optional.

 -- Jakub Wilk <jwilk@debian.org>  Mon, 02 May 2011 19:47:58 +0200

libvigraimpex (1.7.1+dfsg-4) unstable; urgency=low

  * Preserve symlinks when copying object files from one build directory to
    another (closes: #618667).

 -- Jakub Wilk <jwilk@debian.org>  Sun, 20 Mar 2011 15:50:05 +0100

libvigraimpex (1.7.1+dfsg-3) unstable; urgency=low

  * Update the symbols file.
  * On buildds, artificially limit paralellism in order not to run out of RAM.

 -- Jakub Wilk <jwilk@debian.org>  Tue, 15 Mar 2011 15:14:04 +0100

libvigraimpex (1.7.1+dfsg-2) unstable; urgency=low

  * Upload to unstable.
  * Fix patch headers to be in line with DEP-3.
  * Update the symbols file.

 -- Jakub Wilk <jwilk@debian.org>  Sat, 12 Mar 2011 20:29:34 +0100

libvigraimpex (1.7.1+dfsg-1) experimental; urgency=low

  * New upstream release.
    + Drop numpy-double-longdouble.diff, applied upstream.
    + Drop noise-normalization-invariant.diff, applied upstream.
    + Update debian/copyright.
    + Rename libvigraimpex2 to libvigraimpex3, following SONAME bump.
    + Update the symbols file.
    + Include the vigra-config script.
      + Patch it to return correct documentation path.
        [vigra-config-docdir.diff]
      + Update debian/NEWS.
      + Provide a new manual page.
  * Update patch headers.
  * Export LDFLAGS.
  * Remove Win32-only files and pre-built documentation from the .orig
    tarball to save some space.
  * Respect DEB_BUILD_OPTIONS=parallel=N.
  * Respect DEB_BUILD_OPTIONS=noopt.
  * Rename some private variables in debian/rules to make them lowercase.
  * Require that Numpy is usable with every supported Python version.
  * Avoid using SONAMEs in Python extension modules.
  * Convert debian/copyright to the latest DEP-5 version.
  * Install symlinks from /usr/share/doc/libvigraimpex-dev/html to
    /usr/share/doc/libvigraimpex-doc/html.
  * Install symlinks from /usr/share/doc/python-vigra/{html,rst} to
    /usr/share/doc/python-vigra-doc/{html,rst}.
  * Build Python extensions with -fno-strict-aliasing.
  * Remove ‘debian/tmp’ prefix from debian/*.install files.
  * Provide ‘build-arch’ and ‘build-indep’ targets in debian/rules.
  * Use an alternative dependency template in the symbols file to make sure
    that a strict versioned dependency on the shared library will be generated
    for packages using optional (private) symbols.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 20 Feb 2011 23:21:40 +0100

libvigraimpex (1.7.0+dfsg-7) unstable; urgency=low

  * Bump all versions in the symbols file to 1.7.0.
  * Make python-vigra-doc recommend libvigraimpex-doc.
  * Make libvigraimpex-doc suggest python-vigra-doc.
  * Fix documentation generation with Doxygen 1.7.
  * Fix hyperlinks from the C++ library documentation to python-vigra
    documentation (closes: #590069) and the other way round (closes: #590068).
    [docsrc-paths.diff]
  * Stop using g++-4.3 on hppa, as #585925 is fixed now (closes: #588168).

 -- Jakub Wilk <jwilk@debian.org>  Wed, 25 Aug 2010 20:54:45 +0200

libvigraimpex (1.7.0+dfsg-6) unstable; urgency=low

  * Fix generation of dependencies on python-numpy to take epochs into
    account. Use dh_numpy provided by python-numpy if available.

 -- Jakub Wilk <jwilk@debian.org>  Wed, 28 Jul 2010 12:15:09 +0200

libvigraimpex (1.7.0+dfsg-5) unstable; urgency=low

  * To fix ABI breakage, rename binary package from libvigraimpex2ldbl to
    libvigraimpex2 (closes: #589779).
  * Bump standards version to 3.9.1 (no changes needed).
  * Automatically generate versioned dependencies on python-numpy.

 -- Jakub Wilk <jwilk@debian.org>  Mon, 26 Jul 2010 23:17:03 +0200

libvigraimpex (1.7.0+dfsg-4) unstable; urgency=low

  * Bump standards version to 3.9.0 (no changes needed).
  * In order to mitigate bug #586247, add invariant check to
    noiseVarianceListMedianCut(). [noise-normalization-invariant.diff]
  * For the time being, disable failing vigranumpy test for
    noiseVarianceClustering(); see bug #586247.
    [disable-noise-variance-clustering-vigranumpy-test.diff]
  * Update headers for patches that has been applied upstream.

 -- Jakub Wilk <jwilk@debian.org>  Mon, 05 Jul 2010 17:18:21 +0200

libvigraimpex (1.7.0+dfsg-3) unstable; urgency=low

  * New upstream release, which includes backwards-incompatible changes (see
    NEWS.Debian for details).
  * Drop all existing patches: they have been either applied upstream or
    become irrelevant.
  * Upstream added support for HDF5:
    + Add libhdf5-serial-dev to Build-Depends and Depends of
      libvigraimpex-dev.
  * Upstream has implemented new build system on the basis of cmake:
    + Build-depend on cmake.
    + Reimplement debian/rules from scratch; update other files in debian/
      accordingly.
    + Drop vigra-config along with its manual page.
    + No longer provide static library.
  * Simplify makefile for examples.
  * Build Python bindings for VIGRA:
    + Build-depend on python-all-dev, python-numpy, libboost-python-dev,
      python-sphinx.
    + Patch documentation generation system to support out-of-source building.
      [docsrc-out-of-source.diff]
  * Run tests at build time.
    + Build-depend on python-nose.
  * Update the symbols file.
  * Update the watch file.
  * Install JPEG files into examples, too.
  * Update debian/copyright and convert it do DEP-5 format.
  * Fix link in the documentation to cope with the fact that
    tutorial_reference.pdf is compressed in the binary package.
    [gzipped-tutorial-reference.diff]
  * Don't declare the same NumpyArrayValuetypeTraits instance twice if
    sizeof(double) == sizeof(long double); fix FTBFS on such architectures
    (closes: #581688). [numpy-double-longdouble.diff]
  * Use g++-4.3 on hppa for the time being (closes: #582379). g++-4.4 is known
    to generate buggy code, see bug #585925.

 -- Jakub Wilk <jwilk@debian.org>  Wed, 16 Jun 2010 01:12:04 +0200

libvigraimpex (1.6.0+dfsg1-1) unstable; urgency=low

  * Drop README.source, no longer needed.
  * Strip PostScript documents without source from the .orig.tar.gz
    (closes: #580154):
    + Update get-orig-source target.
    + Update links in the HTML documentation to point to copies of these
      documents on VIGRA homepage. [no-offline-ps-doc.diff]

 -- Jakub Wilk <jwilk@debian.org>  Mon, 10 May 2010 19:07:57 +0200

libvigraimpex (1.6.0+dfsg-2) unstable; urgency=low

  * Update symbols file to fix FTBFS on sparc64.
  * Bump standards version to 3.8.4 (no changes needed).
  * Update my e-mail address.
  * Replace (build-)dependency on libjpeg62-dev with libjpeg-dev
    (closes: #569280).
  * Switch to source format 3.0 (quilt). Drop build-dependency on quilt and
    adjust debian/rules accordingly.
  * Fix a typo in src/impex/tiff.cxx (tiff-typo.diff).
  * Update patch descriptions.

 -- Jakub Wilk <jwilk@debian.org>  Thu, 11 Feb 2010 13:02:46 +0100

libvigraimpex (1.6.0+dfsg-1) unstable; urgency=low

  * Use upstream VCS to recreate .orig.tar.gz with the documentation source
    included (closes: #547943).
  * Fix documentation generator to be compatible with recent Doxygen
    (docsrc-doxygen-any-version.diff). Documentation is now regenerated at
    build time.
  * Use sed+tr (rather than lynx) to convert upstream changelog to text.
  * Avoid copy-all-remove-some in the install target.
  * Add Vcs-* fields.
  * Mark more symbols as optional.

 -- Jakub Wilk <ubanus@users.sf.net>  Fri, 09 Oct 2009 21:25:37 +0200

libvigraimpex (1.6.0-2) unstable; urgency=low

  * Make debian/rules safe for parallel builds.
  * Don't install *.la files.
  * Include symbols file.
  * Drop runtime dependency on fftw3.

 -- Jakub Wilk <ubanus@users.sf.net>  Thu, 03 Sep 2009 15:37:01 +0200

libvigraimpex (1.6.0-1) unstable; urgency=low

  * New upstream release (closes: #419431, #540242).
  * New maintainer (closes: #503486).
  * Use quilt to manage patches.
  * Add resampleImage() with different x- and y-factors
    (resample-image-different-xy.diff backported from the upstream VCS;
    closes: #540334).
  * Fix a typo in src/impex/viff.cxx (viff-typo.diff).
  * Sanitize the upstream build system (build-system.diff); run
    autoreconf at build time; remove all autogenerated files at build time.
  * Rewrite debian/rules from scratch; use dh from debhelper 7.
  * Provide a separate documentation package.
  * Update the examples Makefile.
  * Depend of lynx-cur rather than lynx.
  * Bump standards version to 3.8.3 (no changes needed).
  * Drop initial articles from package descriptions.
  * Update the homepage URL.
  * Update debian/copyright.

 -- Jakub Wilk <ubanus@users.sf.net>  Mon, 17 Aug 2009 22:32:07 +0200

libvigraimpex (1.5.0-2) unstable; urgency=low

  * QA upload.
    + Set maintainer to Debian QA Group <packages@qa.debian.org>.
  * Acknowledge NMUs.
  * Add Homepage.
  * Add watch file.
  * Make clean not ignore errors.
  * Update ${Source-Version} depends to ${binary:Version}.
  * Convert debian/copyright to UTF-8.
  * Update fftw3-dev build-depends to libfftw3-dev.
  * Update fftw3 dependency to libfftw3-3.
  * Remove redundant section from libvigraimpex binary.
  * Fix short descriptions.
  * Add proper copyright holders to debian/copyright.
  * Bump debhelper build-dep and compat to 5.
  * Bump Standards Version to 3.8.0.

 -- Barry deFreese <bdefreese@debian.org>  Mon, 02 Feb 2009 17:07:57 -0500

libvigraimpex (1.5.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libvigraimpex2 for the ldbl128 transition (Closes: #430309).

 -- Luk Claes <luk@debian.org>  Sat, 02 Feb 2008 14:56:45 +0000

libvigraimpex (1.5.0-1) unstable; urgency=low

  * New upstream release. Several additions and improvements. Bug
    fixes in code and documentation. Improved the Image Import/Export
    Facilities. Improved support of 64-bit compilers. Added elliptic
    integrals.

 -- Alejandro Sierra <algsierra@gmail.com>  Sat,  9 Dec 2006 11:45:33 -0600

libvigraimpex (1.4.0-1) unstable; urgency=low

  * Initial Release. (Closes: #251616)

 -- Alejandro Sierra <algsierra@gmail.com>  Wed, 21 Dec 2005 14:07:11 -0500
